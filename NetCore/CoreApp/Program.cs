﻿using CoreLibrary;

namespace CoreApp
{
    class Program
    {
        static void Main(string[] args)
        {
            new CoreLibraryClass().HelloWorldMethod();
        }
    }
}
