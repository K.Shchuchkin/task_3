﻿using FrameworkLibrary;

namespace FrameworkApp
{
    class Program
    {
        static void Main(string[] args)
        {
            new FrameworkLibraryClass().HelloWorldMethod();
        }
    }
}
